﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using TMPro;
public class CameraController : MonoBehaviour
{
    [Header("Camera settings")]
    [SerializeField] [Range(45, 89)] float Y_MAX = 85f; //константа
    [SerializeField] [Range(1, 44)] float Y_MIN = 20f;
    public GameObject menu;
    public GameObject signmenu;
    public GameObject trafficmenu;

    public InputField sign_time;

    public InputField redtime;
    public InputField yellowtime;
    public InputField greentime;

    public Tutorial tut;

    [SerializeField] float MouseSens = 4;
    [SerializeField] float MoveSens = 0.1f;

    [SerializeField] float distMin = 1;
    [SerializeField] float distMax = 10;

    float distance = 5.0f;
    float h = 0, v = 0, vert = 0;
    Vector3 camMov = Vector3.zero;

    TrafficLighterController trafficlight;
    SpeedSignController sign;

    [Header("Creator settings")]
    
    int NowI = 0;
    [SerializeField] GameObject PrefP;
    [SerializeField] GameObject Prefl;
    public static CarController Car;
    Color DefoltColor;
    Dictionary<string, Color> BaseColors = new Dictionary<string, Color>
    {
        {"зелёный", new Color(0,1,0) },
        {"красный", new Color(1,0,0) },
        {"синий" , new Color(0,0,1)},
        {"оранжевый", new Color(1,91/255,0)}

    };
    RaycastHit hit;
    
    public void change_sign(){
        int v = int.Parse(sign_time.text);
        sign.transform.gameObject.GetComponentInChildren<TextMeshProUGUI>().text = v.ToString();
        sign.trigger.RegSign(v);
        tut.speedsign();
    }
    public void remove_sign(){
        Destroy(sign.trigger.transform.gameObject);
        Destroy(sign.transform.gameObject);
        sign = null;
        signmenu.SetActive(false);
        tut.speedsign();
    }
    public void change_traffic(){
        trafficlight.Init(int.Parse(redtime.text), int.Parse(yellowtime.text));
        tut.traffic();
    }
    void Start()
    {
       
    }

    
    void Update()
    {

        
        
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Input.GetMouseButtonDown(0)&& Physics.Raycast(ray, out hit))
            {
            if (hit.collider && hit.collider.GetComponentInParent<CarController>())
            {
                if (Car != null)
                {
                    Car.GetComponentInChildren<MeshRenderer>().material.color = DefoltColor;  
                    Car.ClearPathLine();    
                    menu.SetActive(false);
                }
                
                else
                {

                    DefoltColor = hit.collider.GetComponent<MeshRenderer>().material.color;
                }
                if(Car == hit.collider.GetComponentInParent<CarController>()){
                    Car = null;
                    return;
                }
                Car = hit.collider.GetComponentInParent<CarController>();
                trafficmenu.SetActive(false);
                signmenu.SetActive(false);
                sign = null;
                trafficlight = null;
                Car.GetComponentInChildren<MeshRenderer>().material.color = BaseColors["оранжевый"]; // выглядит как красный...
                menu.SetActive(true);
            }else if (hit.collider && hit.collider.GetComponentInChildren<TrafficLighterController>())
            {
                if (Car != null) {
                    Car.GetComponentInChildren<MeshRenderer>().material.color = DefoltColor;
                    Car.ClearPathLine();
                }
                menu.SetActive(false);
                Car = null;
                trafficlight = hit.collider.GetComponentInChildren<TrafficLighterController>();
                trafficmenu.SetActive(true);
                signmenu.SetActive(false);
                sign = null;
            }else if (hit.collider && hit.collider.GetComponentInChildren<SpeedSignController>())
            {
                if (Car != null)
                {
                    Car.GetComponentInChildren<MeshRenderer>().material.color = DefoltColor;
                    Car.ClearPathLine();
                }
                menu.SetActive(false);
                Car = null;
                trafficlight = null;
                trafficmenu.SetActive(false);
                signmenu.SetActive(true);
                sign = hit.collider.GetComponentInChildren<SpeedSignController>();
            }
            else if (hit.collider&&Car!=null)
            {
                tut.carmove();
                Car.NewTarget(hit.point, PrefP);
                
            }    
            
            }


        #region kb mouse input
        if (Input.GetMouseButton(1) && !EventSystem.current.IsPointerOverGameObject())
        {
            h = -Input.GetAxis("Mouse X") * MouseSens;
            v = Input.GetAxis("Mouse Y") * MouseSens;
        }

        if (Input.GetAxis("Mouse ScrollWheel") != 0)
            distance -= Mathf.Sign(Input.GetAxis("Mouse ScrollWheel"));
        distance = Mathf.Clamp(distance, distMin, distMax);

        if (Input.GetKey(KeyCode.LeftShift))
            vert = 1;
        else if (Input.GetKey(KeyCode.LeftControl))
            vert = -1;
        else
            vert = 0;

        camMov = new Vector3(Input.GetAxis("Horizontal"), vert, Input.GetAxis("Vertical"));
        if(Input.GetAxis("Horizontal") != 0 ){
            tut.translate();
        }
        if(Input.GetAxis("Vertical") != 0){
            tut.updown();
        }
        #endregion
    }

    private void LateUpdate()
    {
        this.transform.parent.parent.Rotate(Vector3.up * this.h);
        this.transform.parent.Rotate(Vector3.right * this.v);
        this.transform.parent.rotation = Quaternion.Euler(Mathf.Clamp(this.transform.parent.rotation.eulerAngles.x, this.Y_MIN, this.Y_MAX), this.transform.parent.rotation.eulerAngles.y, 0.0f);
        this.transform.localPosition = -Vector3.forward * this.distance;

        this.transform.parent.parent.Translate(camMov * MoveSens, this.transform.parent.parent);

#warning Please set limits for cam movement
        //this.transform.parent.parent.position = new Vector3(Mathf.Clamp(this.transform.parent.parent.position.x, -200f, 200f), this.transform.parent.parent.position.y, Mathf.Clamp(this.transform.parent.parent.position.z, -200f, 200f));
    }


   
}
