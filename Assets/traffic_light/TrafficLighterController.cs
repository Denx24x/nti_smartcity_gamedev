﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLighterController : MonoBehaviour
{
    int condition = 0;
    List<CarController> CarNextTo = new List<CarController>();
    [SerializeField] GameObject[] Lights;
    float Timer = 0;
    bool StartTimer = false;
    int[] LightSignlTime = new int[2];

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (StartTimer)
        {
            Timer += Time.deltaTime;
            if ((condition == 1 || condition == 3) && (Timer >= LightSignlTime[1]))
            {
                NextCondition();
            }
            else if (Timer >= LightSignlTime[0])
            {
                NextCondition();
            }
        }
    }

    public void Init(int RedTime = 20, int YellowTime = 4, int StartP = 0)
    {
        StartTimer = true;
        condition = StartP-1;
        NextCondition();
        LightSignlTime[0] = RedTime;
        LightSignlTime[1] = YellowTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject!=null && other.gameObject.GetComponentInParent<CarController>())
        {
            CarNextTo.Add(other.gameObject.GetComponentInParent<CarController>());
        
        }
        
        other.gameObject.GetComponentInParent<CarController>().TrafficLightStatus(condition);
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject != null && other.gameObject.GetComponentInParent<CarController>())
        {
            other.gameObject.GetComponentInParent<CarController>().NoOgran();
            CarNextTo.Remove(other.gameObject.GetComponentInParent<CarController>());
            
        }
    }
    void NextCondition()
    {
        condition += 1;

        if (condition == 4)
        {
            condition = 0;
        }
        foreach (GameObject item in Lights)
        {
            item.SetActive(false);
        }
        if (condition == 0)
        {
            
            Lights[2].SetActive(true);
        }
        else if(condition == 2)
        {
            Lights[0].SetActive(true);
        }
        else
        {
            Lights[1].SetActive(true);  
        }
        foreach (CarController item in CarNextTo)
        {
            item.TrafficLightStatus(condition);
        }
        Timer = 0;
    }
    
}
