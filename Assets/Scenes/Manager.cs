﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using UnityEngine.UI;
using SimpleJSON;
using UnityEngine.AI;

[System.Serializable]
public class Building{
    public int id;
    public string type;
    public List<int> position = new List<int>();
    public List<int> size = new List<int>();
    

};

[System.Serializable]
public class Sign{
    public string type;
    public int value;
    public List<int> position = new List<int>();
    public void place(){

    }
};




public class Manager : MonoBehaviour
{
    bool activated = true;
    bool ingame = false;
    public GameObject start_menu;
    public GameObject ingame_menu;
    //public Content data;
    public InputField path_field;
    public GameObject traffic_light;
    public GameObject carmenu;
    public InputField out_field;
    public InputField speed_field;
    public GameObject car;       
    public GameObject road;
    public GameObject stop_sign;
    public GameObject speed_sign;
    public GameObject speed_sign_trigger;
    public List<Building> buildings  = new List<Building>();
    public List<Sign> signs  = new List<Sign>();
    List<NavMeshAgent> cars = new List<NavMeshAgent>(); 
    public GameObject SpeedSign;
    public Text fps;
    List<int> ids = new List<int>();
    public Dictionary<int, List<int>> vert_path = new Dictionary<int, List<int>>();
    public Dictionary<int, List<int>> rev_vert_path = new Dictionary<int, List<int>>();
    public Dictionary<int, List<int>> coordinates = new Dictionary<int, List<int>>();
    // Start is called before the first frame update
    int dst(KeyValuePair<int, int> a, int x1, int y1){
        return Mathf.Abs(a.Key - x1) + Mathf.Abs(a.Value - y1);
    }
    private void Update()
    {
        fps.text  = ((int)(1f / Time.unscaledDeltaTime)).ToString() + " fps";
    }
    KeyValuePair<int, int> get_pnt(int x1, int y1, int x2, int y2, int a, int b){
        if(x2 - x1 != 0){
            int l = Mathf.Min(x1, x2) + 1;
            int r = Mathf.Max(x1, x2) - 1 ;
            if(!(l <= a && a <= r)){
                if(dst(new KeyValuePair<int, int>(l, y1), a, b) < dst(new KeyValuePair<int, int>(r, y1), a, b)){
                    return new KeyValuePair<int, int>(l, y1);
                }else{
                    return new KeyValuePair<int, int>(r, y1);
                }
                
            }
            return new KeyValuePair<int, int>(a, y1);
        }else{
            int l = Mathf.Min(y1, y2) + 1;
            int r = Mathf.Max(y1, y2) - 1;
            if(!(l <= b && b <= r)){
                if(dst(new KeyValuePair<int, int>(x1, l), a, b) < dst(new KeyValuePair<int, int>(x1, r), a, b)){
                    return new KeyValuePair<int, int>(x1, l);
                }else{
                    return new KeyValuePair<int, int>(x1, r);
                }
                
            }
            return new KeyValuePair<int, int>(x1, b);
        }
    }
    void place_road(int x, int y){
        GameObject bf  = GameObject.Instantiate(road);
        bf.transform.SetParent(this.transform);
        bf.gameObject.layer = 8;
        const float size = 0.1f;
        bf.transform.localScale = new Vector3(size, 1, size);
        bf.transform.position = new Vector3(x + 0.5f, 0, y + 0.5f);
        bf.AddComponent<BoxCollider>();
        bf = GameObject.CreatePrimitive(PrimitiveType.Cube);
        bf.GetComponent<BoxCollider>().isTrigger = true;
        bf.AddComponent<OnCrossroad>();
        bf.transform.position = new Vector3(x + 0.5f, 0, y + 0.5f);
        Destroy(bf.GetComponent<MeshRenderer>());
    }
    public float get_sign(float v)
    {
        if (v >= 1)
        {
            return 1;
        }
        return -1;
    }
    void place_sign(Sign v){
        GameObject bf;
        int distance = 999999999;
        int a = 0;
        int b = 0;
         KeyValuePair<int, int> res;
        foreach(int i in vert_path.Keys){
            foreach(int g in vert_path[i]){

                KeyValuePair<int, int> d = get_pnt(coordinates[i][0], coordinates[i][1], coordinates[g][0], coordinates[g][1], v.position[0], v.position[1]);
                if(dst(d, v.position[0], v.position[1]) < distance){
                    distance = dst(d, v.position[0], v.position[1]);
                    res = d;
                    a = i;
                    b = g;
                }
            }
        }

        if(v.type == "SPEED_LIMIT"){
            bf = GameObject.Instantiate(SpeedSign, this.transform);
            bf.GetComponent<SpeedSignController>().RegSign(v.value);
            GameObject trigger = GameObject.Instantiate(speed_sign_trigger, this.transform);
            int dx =  res.Key - v.position[0] ;
            int dy = res.Value - v.position[1];
            trigger.transform.position = new Vector3(res.Key + (dx < 0 ? 1f : 0) , 0, res.Value + (dy < 0 ? 1f : 0));
            trigger.GetComponent<speedcontrolller>().RegSign(v.value);
            bf.GetComponent<SpeedSignController>().trigger = trigger.GetComponent<speedcontrolller>();
        }
        else{
            bf = GameObject.Instantiate(stop_sign, this.transform);
            vert_path[a].Remove(b);
        }
        
        //bf.GetComponent<BoxCollider>().center = new Vector3(res.Key - v.position[0] - 0.5f, 0, -res.Value + v.position[1] - 0.5f);
        bf.transform.position = new Vector3(v.position[0] + 0.5f , 0f, v.position[1] + 0.5f);
    }
    void place_building(Building v){
        
        GameObject bf = GameObject.CreatePrimitive(PrimitiveType.Cube);
        bf.transform.SetParent(this.transform); 
        bf.transform.position = new Vector3(v.position[0] + v.size[0] / 2f, 2.5f, v.position[1] + v.size[1] / 2f);
        bf.transform.localScale = new Vector3(v.size[0], 5, v.size[1]);
        Destroy(bf.GetComponent<BoxCollider>());
    
    }

    void connect_roads(int x1, int y1, int x2, int y2, int p1, int p2){
        
        float dx = x2 - x1;
        float dy = y2 - y1;
        GameObject bf = GameObject.CreatePrimitive(PrimitiveType.Plane);
        bf.transform.SetParent(this.transform);
        if(dx != 0){
            bf.transform.localScale = new Vector3((Mathf.Abs(dx) - 1f) / 10f, 1f, 0.1f);
            if(dx < 0){
                bf.transform.position = new Vector3(x1  + 0.5f + dx / 2f, 0f, y1 + 0.9f);
            }else{
                bf.transform.position = new Vector3(x1  + 0.5f + dx / 2f, 0f, y1  + 0.1f);
            }
            
        }else{
            bf.transform.localScale = new Vector3( 0.1f, 1f, (Mathf.Abs(dy) - 1f) / 10f);
            if(dy < 0){
                bf.transform.position = new Vector3(x1 + 0.1f, 0f, y1  + 0.5f + dy / 2f);
            }else{  
                bf.transform.position = new Vector3(x1 + 0.9f , 0f, y1  + 0.5f + dy / 2f);
            }
        }
        if(!vert_path[p1].Contains(p2)){
            return;
        }
        GameObject path = GameObject.Instantiate(bf, this.transform);
        NavMeshLink enter = path.AddComponent<NavMeshLink>();
        NavMeshLink exit = path.AddComponent<NavMeshLink>();
        enter.bidirectional = false;
        exit.bidirectional = false;
        
        if (dx != 0){
            path.transform.localScale = new Vector3(path.transform.localScale.x - 0.02f, path.transform.localScale.y,  0.02f);
            dx -= get_sign(dx) * 1.01f;
            enter.endPoint = new Vector3(dx / 2f + 0.2f * get_sign(dx), 0, 0);
            enter.startPoint = new Vector3((dx / 2f) - 0.2f * get_sign(dx), 0, 0);
            exit.startPoint = new Vector3(-dx / 2f - 0.2f * get_sign(dx), 0, 0);
            exit.endPoint = new Vector3((-dx / 2f) + 0.2f * get_sign(dx), 0, 0);
        }
        else{
            dy -= get_sign(dy) * 1.01f;
            path.transform.localScale = new Vector3(0.02f, path.transform.localScale.y,  path.transform.localScale.z -0.02f);
            enter.endPoint = new Vector3(0, 0, dy / 2f + 0.2f * get_sign(dy));
            enter.startPoint = new Vector3(0, 0, (dy / 2f) - 0.2f * get_sign(dy));
            exit.startPoint = new Vector3(0, 0, -dy / 2f - 0.2f * get_sign(dy));
            exit.endPoint = new Vector3(0, 0, (-dy / 2f) + 0.2f * get_sign(dy));
        }
        path.AddComponent<BoxCollider>();

        path.gameObject.layer = 8;
        
    }
    void Start()
    {
        start();
    }
    public void place_trafficlight(int id)
    {
        foreach (int to in rev_vert_path[id])
        {
            GameObject bf = GameObject.Instantiate(traffic_light, this.transform);
            bf.transform.position = new Vector3(coordinates[id][0], 0, coordinates[id][1]);
            bf.transform.LookAt(new Vector3(coordinates[to][0], 0, coordinates[to][1]));
            int dx = coordinates[to][0] - coordinates[id][0];
            int dy = coordinates[to][1] - coordinates[id][1];
            bf.transform.position = new Vector3(coordinates[id][0] + 0.5f + (dx != 0 ? 0.5f * get_sign(dx) : 0) - (dy != 0 ? 0.5f * get_sign(dy) : 0), 0, coordinates[id][1] + 0.5f + (dy != 0 ? 0.5f * get_sign(dy) : 0) + (dx != 0 ? 0.5f * get_sign(dx) : 0));
            bf.GetComponentInChildren<TrafficLighterController>().Init(StartP:(dx != 0 ? 0 : 2));
        }
    }
    public void read_file(string path){     
       StreamReader st =  new StreamReader(path);
        var data = JSON.Parse(st.ReadToEnd());
        foreach(var building in data["map"]["buildings"].Values){   
            Building bf = new Building();
            bf.id = building["id"];
            bf.type = building["type"];
            foreach(int g in building["position"].Values){
                bf.position.Add(g);
            }
            foreach(int g in building["size"].Values){
                bf.size.Add(g);
            }
            buildings.Add(bf);
            place_building(bf);
        }
        
        foreach(var vert in data["map"]["road"]["vertexes"].Values){
            coordinates.Add(vert["id"], new List<int>());
            ids.Add(vert["id"]);
            coordinates[vert["id"]].Add(vert["position"][0]);
            coordinates[vert["id"]].Add(vert["position"][1]);
            place_road(vert["position"][0], vert["position"][1]);
        }
        foreach(var edge in data["map"]["road"]["edges"].Values){
            if(!vert_path.ContainsKey(edge["from"])){
                vert_path.Add(edge["from"], new List<int>());
            }
            if (!rev_vert_path.ContainsKey(edge["to"]))
            {
                rev_vert_path.Add(edge["to"], new List<int>());
            }
            if (!rev_vert_path.ContainsKey(edge["from"]))
            {
                rev_vert_path.Add(edge["from"], new List<int>());
            }
            vert_path[edge["from"]].Add(edge["to"]);
            rev_vert_path[edge["to"]].Add(edge["from"]);
        }
        foreach(var sign in data["map"]["signs"].Values){
            Sign bf = new Sign();
            bf.type = sign["type"];
            bf.value = sign["value"];
            foreach(int g in sign["position"].Values){
                bf.position.Add(g);
            }
            signs.Add(bf);
            place_sign(bf);
        }
        foreach(var edge in data["map"]["road"]["edges"].Values){
            connect_roads(coordinates[edge["from"]][0], coordinates[edge["from"]][1], coordinates[edge["to"]][0], coordinates[edge["to"]][1], edge["from"], edge["to"]);
        }
        this.GetComponent<NavMeshSurface>().BuildNavMesh();
        foreach (int id in data["map"]["trafficLights"].Values)
        {
            place_trafficlight(id);
        }
        
        for(int i = 0 ;i < Mathf.Min(ids.Count, 50);i++){
            int ind = ids[i];
            GameObject cr = GameObject.Instantiate(car,  new Vector3(coordinates[ind][0] + 0.5f, 0.1f, coordinates[ind][1] + 0.5f), new Quaternion(0, 0, 0, 0), this.transform);
            
            cars.Add(cr.GetComponent<NavMeshAgent>());
        }
        


    }
    public void start(){
        //try{
            read_file("Assets\\tutor.txt");
            out_field.text = ("");
            activated = false;
            ingame = true;
            //Debug.Log(data);
        //}catch{
            //activated = true;
          //  ingame = false;
        //    out_field.text = ("Невозможно прочитать файл");
        //}
        
        // E:\file.txt
    }

    public void change_speed(){
        float v = float.Parse(speed_field.text) / 36f;
        if(v < 0){
            return;
        }
        CameraController.Car.DefaultSpeed  = v;
        CameraController.Car.MyNavMeshAgent.speed = v;
    }
    public void to_main(){

        foreach (Transform child in this.transform) {
            GameObject.Destroy(child.gameObject);
        }
        ingame = false;
        activated = true;
        carmenu.SetActive(false);
        vert_path.Clear();
        coordinates.Clear();
        signs.Clear();
        buildings.Clear();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && ingame){
            activated = !activated;
        }
        if(activated){
            if(!ingame){
                start_menu.SetActive(true);
                ingame_menu.SetActive(false);
            }else{
                ingame_menu.SetActive(true);
                start_menu.SetActive(false);
            }
         }else{
            ingame_menu.SetActive(false);
            start_menu.SetActive(false);
        }
        foreach(NavMeshAgent i in cars){
            if(!i.pathPending && i.remainingDistance <= 0.5f){
                int ind = ids[UnityEngine.Random.Range(0, ids.Count - 1)];
                i.SetDestination(new Vector3(coordinates[ind][0] + 0.5f, 0.1f, coordinates[ind][1] + 0.5f));
            }
        }
    }
}
